<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home');
// });

Route::resource('product','ProductController');
//Route::resource('/category','CategoryController');

Route::get('/', 'PagesController@home');


Route::get('/shop', 'ShopController@index')->name('shop.index');
Route::get('/cart.index', 'CartController@index')->name('cart.index');
Route::post('/cart.store', 'CartController@store')->name('cart.store');
Route::delete('/cart/{product}', 'CartController@destroy')->name('cart.destroy');
Route::post('/cart.switchToSaveForLater/{product}', 'CartController@switchToSaveForLater')->name('cart.switchToSaveForLater');

Route::delete('/saveForLater/{product}', 'SaveForLaterController@destroy')->name('saveForLater.destroy');
Route::post('/saveForLater.switchToCart/{product}', 'SaveForLaterController@switchToCart')->name('saveForLater.switchToCart');

Route::get('empty', function(){
    Cart::instance('saveForLater')->destroy();
});

Route::get('/product.create', 'ProductController@create');
Route::get('/product.show/{product}', 'ProductController@show');
Route::delete('/product.delete/{id}', 'ProductController@destroy');


Route::get('/cart', 'ShopController@show');
Route::get('/order.index', 'OrderController@index')->name('order.index')->middleware('auth');
Route::post('/order', array('as' => 'store', 'uses' => 'OrderController@store'));
Route::get('/order.show', 'OrderController@show')->name('order.show');
Route::get('/order.view/{product}', 'OrderController@view');

Route::get('/guestOrder.index', 'OrderController@index')->name('guestOrder.index');


Route::get('/admin/category.create', 'CategoryController@create');


Route::prefix('admin')->group(function() {
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/dashboard', 'AdminController@index')->name('admin.dashboard');
});

Auth::routes();

Route::get('/dashboard', 'DashboardController@index');
Route::post('/product', 'ProductController@store');
Route::post('/category', 'CategoryController@store');
Route::get('/category.create', 'CategoryController@create');

Route::get('/product/{product}/edit', 'ProductController@edit');
Route::put('product/{product}', 'ProductController@update');

Route::middleware('auth')->group(function(){
    Route::get('/my-profile', 'UsersController@edit')->name('users.edit');
    Route::post('/my-profile', 'UsersController@update')->name('users.update');

});
