<?php

namespace App\Http\Controllers;

use App\Shop;
use Illuminate\Http\Request;
use App\Category;
use App\Product;
use DB;

class ShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        //$products = Product::orderBy('created_at', 'desc')->paginate(16);        
        //$products = DB::table('products')->where('category_id', '=', 26)->paginate(16);
        if(request()->category){
            $products = Product::with('categories')->whereHas('categories', function($query){
                $query->where('name', request()->category);
            })->paginate(16);
            $categories = Category:: all();
            $categoryName = $categories->where('name', request()->category)->first()->name;
        }else{
            $products = Product::orderBy('created_at', 'desc')->paginate(16); 
            //$products = Product::inRandomOrder()->take(16)->get();
            $categories = Category:: all();
            $categoryName = 'Products';
        }

        return view ('/shop')->with([
            'products'=>$products,
            'categories'=>$categories,
            'categoryName' => $categoryName,
        ]);
       
    }

    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Shop  $shop
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       return view('/cart');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Shop  $shop
     * @return \Illuminate\Http\Response
     */
    public function edit(Shop $shop)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Shop  $shop
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Shop $shop)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Shop  $shop
     * @return \Illuminate\Http\Response
     */
    public function destroy(Shop $shop)
    {
        //
    }
}
