<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use RealRashid\SweetAlert\Facades\Alert;
use App\Orders;
use App\OrderProduct;
use Gloudemans\Shoppingcart\Facades\Cart;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('order')->with('products', $products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //try{

            $emailValidation = auth()->user() ? 'required|email' : 'required|email|unique:users';

            $this->validate($request, [
                'firstName' => 'required',
                'lastName' => 'required',
                'email' => $emailValidation,
                'address' => 'required',
                'phone' => 'required',
                'state'=>'required',
                'zip' => 'required'
            ]);

            $order = Orders::create([
                'user_id' => auth()->user() ? auth()->user()->id : null,
                'firstName' => $request->firstName,
                'lastName' => $request->lastName,
                'email' => $request->email,
                'address' => $request->address,
                'phone' => $request->phone,
                'country' => 'Srbija',
                'state' => $request->state,
                'zip' => $request->zip,
            ]);


            foreach(Cart::content() as $item){
                OrderProduct::create([
                    'orders_id' => $order->id,
                    'product_id' => $item->id,
                    'quantity' => $item->qty,
                    'total'=> $item->total,

                ]);
            }

            Cart::instance('default')->destroy();

            return redirect('/shop')->with('toast_success', 'Thank you! Your order was successfuly accepted!');

    }

    public function messages()
    {
        return [
            'email.unique' => 'You already have an account with this email address. Please login to continue.'
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //$orders = Orders::all();
        $orders = Orders::orderBy('created_at','desc')->with('products')->get();  
        $total = 0;  
        return view('order.show', compact('orders'));
    }


    // public function view($id){
       
    
    //     $order = Orders::find($id);
    //     $products = $order->product;
        
        
    //     return view('order.view')->with(['products' => $products, 'order' => $order]);
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
