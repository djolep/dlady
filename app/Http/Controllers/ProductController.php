<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use Illuminate\Http\Request;
use Auth;
use Storage;
use RealRashid\SweetAlert\Facades\Alert;
use Gloudemans\Shoppingcart\Facades\Cart;


class ProductController extends Controller
{
    
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::orderBy('created_at','desc')->get();
        return view('/shop')->with('products', $products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::guard('admin')->check()){
            //$categories = Category::all();
            return view('product.create', [
                'categories' => Category::all()
            ]);
        }else{
            return redirect('/admin/login');
        }

        
        
    }

   


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{

       
                $this->validate($request, [
                    'name' => 'required',
                    'subtitle' => 'required',
                    'description' =>'required',
                    'price' => 'required',
                    'image' => 'image|nullable',
                    'discount'=>'integer|nullable',
                    'categories' => 'exists:categories, id'
                ]);

                if ($request->hasFile('image')){
                    $filenameWithExt = $request->file('image')->getClientOriginalName();
                    //Get just filename
                    $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                    //Get just extension
                    $extension = $request->file('image')->getClientOriginalExtension();
                    //Filename to store
                    $fileNameToStore = $filename.'_'.time().'.'.$extension;
                    $path = $request->file('image')->storeAs('public/Product_images', $fileNameToStore);

                } else {
                    $fileNameToStore='noimage.jpg';
                }

                $product = new Product;
                $product->category_id = $request->input('category_id');
                $product->name = $request->input('name');
                $product->subtitle = $request->input('subtitle');
                $product->design = $request->input('design');
                $product->color = $request->input('color');
                $product->delivery = $request->input('delivery');
                $product->description = $request->input('description');
                $product->price = $request->input('price');
                $product->discount = $request->input('discount');
                $product->image = $fileNameToStore;


                
                $product->save();
                $product->categories()->attach(request('category_id'));
               
            

                return back()->with('toast_success', 'Product added successful!');
            }
            catch (\Exception $e) {
                return $e->getMessage();
            }

       

    }
    // protected function create(array $data)
    // {
    //     return Product::create([
    //         'category' => $data['category'],
    //         'name' => $data['name'],
    //         'subtitle' => $data['subtitle'],
    //         'description' => $data['description'],
    //         'price' => $data['price'],
            
    //     ]);
    // }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);
        return view('product.show')->with('product', $product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        
        $product = Product::find($id);
        $categories = Category::all();
         return view('product.edit')->with(['product'=> $product, 'categories' => $categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'subtitle' => 'required',
            'description' =>'required',
            'price' => 'required',
            'image' => 'image|nullable',
        ]);

        if ($request->hasFile('image')){
            $filenameWithExt = $request->file('image')->getClientOriginalName();
            //Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //Get just extension
            $extension = $request->file('image')->getClientOriginalExtension();
            //Filename to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            $path = $request->file('image')->storeAs('public/Product_images', $fileNameToStore);

        } 

        
        $product = Product::find($id);
        $product->category_id = $request->input('category_id');
        $product->name = $request->input('name');
        $product->subtitle = $request->input('subtitle');
        $product->design = $request->input('design');
        $product->color = $request->input('color');
        $product->delivery = $request->input('delivery');
        $product->description = $request->input('description');
        $product->price = $request->input('price');
        $product->discount = $request->input('discount');
        if($request->hasFile('image')){
            $product->image = $fileNameToStore;
        }
        
        $product->save();

        
       

        return back()->with('success', 'Product updated successful!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);

        if($product->image != 'noimage.jpg'){
            //Delete image
            Storage::delete('public/Product_images/'.$product->image);
        }

        //alert()->question('Are you sure?','You won\'t be able to revert this!')->showCancelButton()->showConfirmButton()->focusCancel(true);

        $product->delete();
        return back()->with('success', 'Product removed successful!');
    }
}


