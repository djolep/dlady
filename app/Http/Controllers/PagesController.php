<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Product;
use DB;

class PagesController extends Controller
{
    public function home()
    {
        $categories = Category::latest()->take(6)->get();
        return view('/home')->with('categories', $categories);
    }

      
        
}
