<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Validator;
use Storage;

class SaveForLaterController extends Controller
{
 

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cart::instance('saveForLater')->remove($id);

        return back()->with('toast_success', 'Item has been removed');
    }


    /**
     * Switch item from Saved for later to cart.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function switchToCart($id)
    {
        $product = Cart::instance('saveForLater')->get($id);

        Cart::instance('saveForLater')->remove($id);

        $duplicates = Cart::instance('default')->search(function ($cartItem, $rowId) use ($id) {
            return $rowId === $id;
        });

        if($duplicates->isNotEmpty()){
            return redirect()->route('cart.index')->with('toast_success', 'Item is already in your cart!');
        }

        Cart::instance('default')->add($product->id, $product->name, 1 , $product->price)
            ->associate('app\Product');

            return redirect()->route('cart.index')->with('toast_success', 'Item was been moved to Cart!');
    }
}
