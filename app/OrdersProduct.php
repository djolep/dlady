<?php

namespace App;
use App\Product;

use Illuminate\Database\Eloquent\Model;
use Gloudemans\Shoppingcart\Facades\Cart;


class OrderProduct extends Model
{
    protected $table = 'orders_product';

    protected $fillable = ['orders_id', 'product_id', 'quantity','total'];


    
}
