<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    public function category(){
        return $this->belongsToMany('App\Category');
    }

    public function Product(){
        return $this->belongsToMany('App\Product');
    }


}
