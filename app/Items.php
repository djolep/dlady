<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Items extends Model
{
    protected $fillable=['orders_id', 'product_id', 'product_name', 'product_design', 'product_image', 'product_price'];
}
