<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;
use App\Orders;

class Product extends Model
{
   
//protected $fillable = ['category_id', 'name', 'subtitle', 'description', 'price', 'discount', 'image', 'discounted_price'];

    protected $table = 'products';

    public $primaryKey = 'id';
    
    public $timestamps = true;


    public function categories(){
        return $this->belongsToMany('App\Category');
        
        
    }

    // public function orders() {
    //     return $this->belongsToMany('App\Orders');
    // }

    public function orderproduct() {
        return $this->belongsTo('App\OrderProduct');
    }

    // public function discount($discounted_price){

    //     return $this->price * (1 - $this->discount / 100);
    // }


}

//old price
//Product::discount($discounted_price); //price with discount

