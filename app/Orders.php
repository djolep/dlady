<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Gloudemans\Shoppingcart\Facades\Cart;
use App\Product;

class Orders extends Model
{
    protected $fillable = ['firstName', 'lastName', 'email', 'address', 'phone', 'country', 'state', 'zip' ];


    public function user()
    {
        return $this->belongsTo('App\User');
    }


    public function products()
    {
        return $this->belongsToMany('App\Product')->withPivot(['quantity', 'total']);
    }

    public function users()
    {
        return $this->belongsTo('App\User');
    }
}
