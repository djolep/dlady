@extends('layouts.app')

@section('content')
<div class="wrap" style="width: 100%;">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header ">Admin Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
					 <div class="sidebar">
						
					</div>
                   <button class="btn btn-success"><a class="text-white" href="/product.create">Add Product</a></button>
                   <button class="btn btn-success"><a class="text-white" href="/category.create">Add category</a></button>
                   <button class="btn btn-success">Add Best Product</button>
                   <button class="btn btn-success">Settings</button>
                   <button class="btn btn-success"><a class="text-white" href="/order.show">Orders</a></button>
                    

                </div>

                <div class="wrap">
                    <table class="table table-striped">
                        <thead class="thead-dark">
                          <tr>
                            <th scope="col">Image</th>
                            <th scope="col">Product name</th>
                            <th scope="col">Design</th>
                            <th scope="col">Color</th>
                            <th scope="col">Description</th>
                            <th scope="col">Price</th>
                            <th scope="col">Discount</th>
                            <th scope="col">Best seller</th>
                            <th scope="col">Update</th>
                            <th scope="col">Delete</th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach ($products as $product)                  
                                <tr>
                                <th scope="row"><img src="/storage/Product_images/{{$product->image}}" style="width: 60px; height: 60px;" alt=""></th>
                                    <td>{{$product->name}}</td>
                                    <td>{{$product->design}}</td>
                                    <td>{{$product->color}}</td>
                                    <td>{{$product->description}}</td>
                                    <td>{{$product->price}} din</td>
                                    <td>{{$product->discount}} %</td>
                                    <td> <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2"></td>
                                    <td><button class="btn btn-warning" ><a style="color:white;" href="/product/{{$product->id}}/edit">Edit</a></button></td>
                                    <td>
                                        <form action="{{route('product.destroy', $product->id)}}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger">Delete</button>
                                        </form>
                                    </td>
                                </tr>     
                            @endforeach
                        </tbody>
                      </table>
                </div>   
                <div class="alert ">
                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            {{ session()->get('message') }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>




@endsection
