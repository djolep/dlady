@extends('layouts.app')
@section('title',"Cart")
@section('content')



<div class="container" >
    <div class="card shopping-cart">
             <div class="card-header text-light">
                 <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                 Shopping cart
                 <a href="/shop" class="btn btn-outline-info btn-sm pull-right" style="color: white;">Continue shopping</a>
                 <div class="clearfix"></div>
             </div>
                <div class="alert" role="alert">
                    @if(session()->has('success_message'))
                        <div class="alert alert-success text-center">
                            {{ session()->get('success_message') }}
                        </div>
                    @endif

                    @if(count($errors) > 0)
                        <div class="alert alert-success">
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
    @if(Cart::count() > 0)

        <h4 class="text-center">{{ Cart::count()}} item(s) in Shoping Cart</h4>

             <div class="card-body">
                 @foreach (Cart::content() as $product)
                     <!-- PRODUCT -->
                     <div class="row">
                         <div class="col-12 col-sm-12 col-md-2 text-center">
                                 <a href="/product.show/{{$product->id}}"><img class="img-responsive" src="/storage/Product_images/{{$product->model->image}}" alt="preview" width="120" height="80"></a>
                         </div>
                         <div class="col-12 text-sm-center col-sm-12 text-md-left col-md-6">
                             <a href="/product.show/{{$product->id}}"><h4 class="product-name"><strong>{{$product->model->name}}</strong></h4></a>
                             <h4>
                                 <small>{{$product->model->design}}</small>
                             </h4>
                         </div>
                         <div class="col-12 col-sm-12 text-sm-center col-md-4 text-md-right row">
                             <div class="col-3 col-sm-3 col-md-6 text-md-right" style="padding-top: 5px">
                                 <h6><strong>{{$product->price }} din<span class="text-muted"> x</span></strong></h6>
                             </div>
                             <div class="col-4 col-sm-4 col-md-4">
                                 <div class="quantity">
                                     {{-- <input type="button" value="+" class="plus"> --}}
                                     <input type="number" step="1" max="99" min="1" value="1" title="Qty" class="qty"
                                            size="4">
                                     {{-- <input type="button" value="-" class="minus"> --}}
                                 </div>
                             </div>
                             <div class="col-2 col-sm-2 col-md-2 text-right">
                                 {{-- <button href="" type="button" class="btn btn-outline-danger btn-xs">
                                     <i class="fa fa-trash" aria-hidden="true"></i>
                                 </button> --}}
                                <form action="{{route('cart.destroy', $product->rowId)}}" method="POST">
                                    {{csrf_field()}}
                                    {{method_field('DELETE')}}
                                    <button href="" type="submit" class="btn btn-outline-danger btn-xs">
                                        <i class="fas fa-trash" aria-hidden="true"></i>
                                    </button>
                                </form>
                                

                                <form action="{{route('cart.switchToSaveForLater', $product->rowId)}}" method="POST">
                                    {{csrf_field()}}
                                    
                                    <button style="margin-top: 10px;" href="" type="submit" class="btn btn-outline-danger btn-xs">
                                        <i class="fas fa-save" aria-hidden="true"></i>
                                    </button>
                                </form>
                                {{-- <button style="margin-top: 10px;" href="" type="button" class="btn btn-outline-danger">
                                    <i class="fas fa-save"></i>
                                </button> --}}
                             </div>
                         </div>
                     </div>
                     <hr>
                 @endforeach  
                
                 {{-- <div class="pull-right">
                     <a href="" class="btn btn-outline-secondary pull-right">
                         Update shopping cart
                     </a>
                 </div> --}}
             </div>

        @else
            <h2 class="text-center">No items in Cart</h2>
    @endif
             <div class="card-footer">
                 <div class="pull-right" style="margin: 10px">
                     <a href="{{route('order.index')}}"  class="btn btn-success pull-right">Checkout</a>
                     <div class="pull-right" style="margin: 5px">
                         Total price: <b>{{Cart::total()}} din</b>
                     </div>
                 </div>
             </div>
         </div>
 </div>

<div style="margin-top: 70px;" class="container">
 <div class="card shopping-cart">
    <div class="card-header  text-light">
        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
        Save for later
        {{-- <a href="/shop" class="btn btn-outline-info btn-sm pull-right">Continue shopping</a> --}}
        <div class="clearfix"></div>
    </div>
       <div class="alert" role="alert">
           @if(session()->has('warning_message'))
               <div class="alert alert-success text-center">
                   {{ session()->get('warning_message') }}
               </div>
           @endif

           @if(count($errors) > 0)
               <div class="alert alert-success">
                   <ul>
                       @foreach($errors->all() as $error)
                           <li>{{$error}}</li>
                       @endforeach
                   </ul>
               </div>
           @endif
       </div>

       @if(Cart::instance('saveForLater')->count() > 0)

                     <h4 class="text-center">{{ Cart::instance('saveForLater')->count()}} item(s) in Save for Later</h4>
                     <div class="card-body">
                        @foreach (Cart::instance('saveForLater')->content() as $product)
                            <!-- PRODUCT -->
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-2 text-center">
                                        <a href="/product.show/{{$product->id}}"><img class="img-responsive" src="/storage/Product_images/{{$product->model->image}}" alt="preview" width="120" height="80"></a>
                                </div>
                                <div class="col-12 text-sm-center col-sm-12 text-md-left col-md-6">
                                    <a href="/product.show/{{$product->id}}"><h4 class="product-name"><strong>{{$product->model->name}}</strong></h4></a>
                                    <h4>
                                        <small>{{$product->model->design}}</small>
                                    </h4>
                                </div>
                                <div class="col-12 col-sm-12 text-sm-center col-md-4 text-md-right row">
                                    <div class="col-3 col-sm-3 col-md-6 text-md-right" style="padding-top: 5px">
                                        <h6><strong>{{$product->model->price }} din<span class="text-muted"> x</span></strong></h6>
                                    </div>
                                    <div class="col-4 col-sm-4 col-md-4">
                                        <div class="quantity">
                                            {{-- <input type="button" value="+" class="plus"> --}}
                                            <input type="number" step="1" max="99" min="1" value="1" title="Qty" class="qty"
                                                   size="4">
                                            {{-- <input type="button" value="-" class="minus"> --}}
                                        </div>
                                    </div>
                                    <div class="col-2 col-sm-2 col-md-2 text-right">
                                        {{-- <button href="" type="button" class="btn btn-outline-danger btn-xs">
                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                        </button> --}}
                                       <form action="{{route('saveForLater.destroy', $product->rowId)}}" method="POST">
                                           {{csrf_field()}}
                                           {{method_field('DELETE')}}
                                           <button href="" type="submit" class="btn btn-outline-danger btn-xs">
                                               <i class="fa fa-trash" aria-hidden="true"></i>
                                           </button>
                                       </form>
                                       
       
                                       <form action="{{route('saveForLater.switchToCart', $product->rowId)}}" method="POST">
                                           {{csrf_field()}}
                                           
                                           <button style="margin-top: 10px;" href="" type="submit" class="btn btn-outline-danger btn-xs">
                                               <i class="fas fa-shopping-cart"></i>
                                           </button>
                                       </form>
                                       {{-- <button style="margin-top: 10px;" href="" type="button" class="btn btn-outline-danger">
                                           <i class="fas fa-save"></i>
                                       </button> --}}
                                    </div>
                                </div>
                            </div>
                            <hr>
                        @endforeach
                     </div>
                     @else
                        <h4 class="text-center">You have no items Saved for Later</h4>
                     @endif



 </div>
</div>  
@endsection