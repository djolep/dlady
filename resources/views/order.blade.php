@extends('layouts.app')

@section('content')
<div class="container">
  
    <div class="py-5 text-center">
      <h2>Checkout form</h2>
      <p class="lead">Below is an example form built entirely with Bootstrap’s form controls. Each required form group has a validation state that can be triggered by attempting to submit the form without completing it.</p>
    </div>

    <div id="custom-target" class="text-center alert">
      @if($errors->any())
        {!! implode('', $errors->all('<div class="alert alert-warning">:message</div>')) !!} <hr>
    @endif
    </div>
{{-- 
    @if(count($errors) > 0)
      <div class="spacer"></div>
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{!! $error !!}}</li>
          @endforeach
        </ul>
      </div>
    @endif --}}
    
   <form id="order-form" action="/order" method="POST" enctype="multipart/form-data">
          {{csrf_field()}}
      <div class="row">
        <div class="col-md-4 order-md-2 mb-4">
          <h4 class="d-flex justify-content-between align-items-center mb-3">
            <span class="text-muted">Your cart</span>
            @if(Cart::instance('default')->count() > 0)
            <span class="badge badge-secondary badge-pill">{{Cart::instance('default')->count()}}</span>
            @endif
          </h4>
          <ul class="list-group mb-3">
              @foreach (Cart::content() as $product)
                  <li class="list-group-item d-flex justify-content-between lh-condensed">                      
                    <div>
                      <img style="width:50px; height: 50px;" src="/storage/Product_images/{{$product->model->image}}" alt="">
                        <h6 class="my-0">{{$product->name}}</h6>
                        <small class="text-muted">{{$product->model->design}}</small>
                    </div>
                        <span class="text-muted">{{$product->price}}.00 din</span>                      
                  </li> 
              @endforeach
          
          <li class="list-group-item d-flex justify-content-between">
            <span>Total (DIN)</span>
            <strong>{{Cart::total()}} din</strong>
          </li>
        </ul>
    
        </div>
        
        
        <div class="col-md-8 order-md-1">
          <h4 class="mb-3">Billing address</h4>
          
            <div class="row">
              <div class="col-md-6 mb-3">
                <label for="firstName">First name</label>
                <input type="text" name="firstName" class="form-control" id="firstName" placeholder="" value="{{ old('firstName') }}" required>
                <div class="invalid-feedback">
                  Valid first name is required.
                </div>
              </div>
              <div class="col-md-6 mb-3">
                <label for="lastName">Last name</label>
                <input type="text" name="lastName" class="form-control" id="lastName" placeholder="" value="{{ old('lastName') }}" required>
                <div class="invalid-feedback">
                  Valid last name is required.
                </div>
              </div>
            </div>
        
            <div class="mb-3">
              <label for="email">Email <span class="text-muted"></span></label>
              @if(auth()->user())
                <input type="email" name="email" class="form-control" id="email" placeholder="you@example.com"  value="{{ auth()->user()->email }}" readonly>
              @else
                <input type="email" name="email" class="form-control" id="email" placeholder="you@example.com" required value="{{ old('email') }}">
              @endif
              <div class="invalid-feedback">
                Please enter a valid email address for shipping updates.
              </div>
            </div>
    
            <div class="mb-3">
              <label for="address">Address</label>
              <input type="text" name="address" class="form-control" id="address" placeholder="1234 Main St" required value="{{ old('address') }}">
              <div class="invalid-feedback">
                Please enter your shipping address.
              </div>
            </div>
    
            <div class="mb-3">
              <label for="phone">Phone number <span class="text-muted"></span></label>
              <input type="text" name="phone" class="form-control" id="" placeholder="Your contact phone" required value="{{ old('phone') }}">
            </div>
    
            <div class="row">
              <div class="col-md-5 mb-3">
                <label for="country">Country</label>
                <input type="text" name="country" class="form-control" id="country" value="Serbia" required >
                <div class="invalid-feedback">
                  Please select a valid country.
                </div>
              </div>
              <div class="col-md-4 mb-3">
                <label for="state">State</label>
                <input type="text" name="state" class="form-control" id="state" placeholder="Your city" value="{{ old('state') }}" required>
                <div class="invalid-feedback">
                  Please provide a valid state.
                </div>
              </div>
              <div class="col-md-3 mb-3">
                <label for="zip">Zip</label>
                <input type="text" name="zip" class="form-control" id="zip" placeholder="" required value="{{ old('zip') }}">
                <div class="invalid-feedback">
                  Zip code required.
                </div>
              </div>
            </div>
              <input type="hidden" name="product_id" value="{{$product->id}}">

              <input type="hidden" name="product_name" value="{{$product->name}}">

              <input type="hidden" name="product_design" value="{{$product->design}}">

              <input type="hidden" name="product_image" value="{{$product->image}}">

              <input type="hidden" name="product_price" value="{{$product->price}}">

              <input type="hidden" name="product_total" value="{{Cart::total()}}">
            <hr class="mb-4">

            
            
            <input type="submit" id="complete-order" class="btn btn-primary btn-lg btn-block" value="Continue to checkout">
      </div>
    </form>

    </div>
@endsection