<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title >@yield('title',"Dlady")</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@200&display=swap" rel="stylesheet">
    <!-- <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet"> -->
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script&display=swap" rel="stylesheet">
    <link href="http://fonts.cdnfonts.com/css/adine-kirnberg" rel="stylesheet">
                
    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="/css/style.css">
    {{-- <link rel="stylesheet" href="/js/main.js"> --}}
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://kit.fontawesome.com/01999e5283.js" crossorigin="anonymous"></script>
</head>
<body>    
    <div id="app">
        <!-- <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand logo" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse"  id="navbarSupportedContent">
                   
                    <ul class="nav navbar-nav mr-auto meny">

                        <li class="nav-item">
                            <a class="nav-link" href="/">Home</a>
                        </li>
                                                
                        <li class="nav-item">
                            <a class="nav-link" href="/shop">Shop</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="">Blog</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="">About us</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="">Contact</a>
                        </li>


                    </ul>

                    
                    <ul class="navbar-nav ml-auto">
                        
                        @if (Auth::guard('admin')->check() || Auth::guard()->check())
                            <li class="nav-item dropdown">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        @if (Auth::guard('admin')->check())
                                            Admin
                                        @else 
                                            {{ Auth::user()->name }} 
                                        @endif
                                        <span class="caret"></span>
                                    </a>
    
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        @if (Auth::guard('admin')->check())
                                            <a class="dropdown-item" href="/admin/dashboard">Dashboard</a>
                                        @else   
                                            <a class="dropdown-item" href="{{route('users.edit')}}">My-Profile</a> 
                                        @endif
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>
    
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                        @else
                        <li class="nav-item dropdown">
                                <a id="linkDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ __('Login') }}
                                    <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu" aria-labelledby="linkDropdown">
                                    <a class="dropdown-item" href="{{ route('login') }}">
                                        {{ __('User') }}
                                    </a>
                                    <a class="dropdown-item" href="{{ route('admin.login') }}">
                                        {{ __('Admin') }}
                                    </a>
                                </div>
                            </li>
                            <li><a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a></li>
                        @endif
                        
                    </ul>
                </div>
            </div>
            <div class="navbar-nav ">
                {{-- <li class="nav-item border rounded-circle mx-2 search-icon">
                    <i class="fas fa-search p-2"></i>
                </li> --}}

                <li class="nav-item ">
                <a href="{{ route('cart.index' )}}"><i class="fas fa-shopping-cart fa-2x "></i></a>                
                </li>
                @if(Cart::instance('default')->count() > 0)
                <span class="cart-count text-center">{{Cart::instance('default')->count()}}</span> 
                @endif
            </div>
        </nav> -->
    <header>
        <div class="container">
            <nav class="nav">
                <ul class="nav-list nav-list-mobile">
                    <li class="nav-item">
                        <div class="mobile-menu">
                            <span class="line line-top"></span>
                            <span class="line line-bottom"></span>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a href="/" class="nav-link nav-link-apple">Dlady</a>
                    </li>
                    <li class="nav-item">
                        <a href="/" class="nav-link nav-link-bag"></a>
                    </li>
                </ul>
                <!-- /.nav-list nav-list-mobile -->

                <ul class="nav-list nav-list-larger">
                    <li class="nav-item nav-item-hidden">
                        <a href="/" class="nav-link nav-link-apple">Dlady</a>
                    </li>
                    <li class="nav-item">
                        <a href="/" class="nav-link">Home</a>
                    </li>
                    <li class="nav-item">
                        <a href="/shop" class="nav-link">Shop</a>
                    </li>
                    <li class="nav-item">
                        <a href="/" class="nav-link">About</a>
                    </li>
                    <li class="nav-item">
                        <a href="/" class="nav-link">Contact</a>
                    </li>
                    
                    
                    <li class="nav-item">
                        <a href="/" class="nav-link nav-link-search"></a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('cart.index' )}}" class="nav-link nav-link-bag"></a>
                    </li>
                    @if(Cart::instance('default')->count() > 0)
                            <span class="cart-count text-center">{{Cart::instance('default')->count()}}</span> 
                          @endif

                    @if (Auth::guard('admin')->check() || Auth::guard()->check())
                            <li class="nav-item dropdown">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        @if (Auth::guard('admin')->check())
                                            Admin
                                        @else 
                                            {{ Auth::user()->name }} 
                                        @endif
                                        <span class="caret"></span>
                                    </a>
    
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        @if (Auth::guard('admin')->check())
                                            <a class="dropdown-item" href="/admin/dashboard">Dashboard</a>
                                        @else   
                                            <a class="dropdown-item" href="{{route('users.edit')}}">My-Profile</a> 
                                        @endif
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>
    
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                        @else
                        <li class="nav-item dropdown">
                                <a id="linkDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ __('Login') }}
                                    <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu" aria-labelledby="linkDropdown">
                                    <a class="dropdown-item" href="{{ route('login') }}">
                                        {{ __('User') }}
                                    </a>
                                    <a class="dropdown-item" href="{{ route('admin.login') }}">
                                        {{ __('Admin') }}
                                    </a>
                                </div>
                            </li>
                            <li><a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a></li>
                        @endif
                </ul>
                <!-- /.nav-list nav-list-larger -->
            </nav>
        </div>
    </header>

       @yield('header')

        <main class="py-4">
            @yield('content')
            
        </main>
    </div>
    <!-- <script src="js/main.js"></script> -->
    <script type="text/javascript">
        const selectElement = (element) => document.querySelector(element);

        selectElement('.mobile-menu').addEventListener('click', ()  => {
        selectElement('header').classList.toogle('active'); 
        
});
    </script>
    <script type="text/javascript">   
        $(document).ready(function () {   
        $('input#complete-order').on('click', function () {   
        var myForm = $("form#order-form");   
        if (myForm) {   
        $(this).prop('disabled', true);   
        $(myForm).submit();   
        }   
        });   
        });   
        </script>  

        <script type="text/javascript">
            var tavle = document.getElementById('table'), sumVal = 0;

            for(var i = 1; i < table.rows.length; i++){
                sumVal = sumVal + parseInt(table.rows[i].cells[2].innerHTML);
            }

            document.getElementById('val').innerHTML = 'Total: ' +sumVal;
            console.log(sumVal);

        </script>

        <!-- <script type="text/javascript">
            $(function() {

               var TotalValue = 0;

               $("tr .subjects").each(function(index,value){
                 currentRow = parseFloat($(this).text());
                 TotalValue += currentRow
               });

               document.getElementById('total').innerHTML = 'Total: ' +TotalValue;

            });
        </script> -->

    <!-- <script>
        $(document).ready(function(){
            $("tr").each(function(){
                var total = 0;
                $ (this).find('.subjects').each(function(){
                    var marks = $(this).text();
                    if(marks.length !== 0){
                        total += parseFloat(marks);                      
                    }
                });
                $(this).find('#total').html('Total: ' +total);
            });
        });
    </script> -->


    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    @include('sweetalert::alert')
</body>
</html>
