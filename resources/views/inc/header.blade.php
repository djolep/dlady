@extends('layouts.app')

@section('header')

        <div class="container">
            <nav class="nav">
                <ul class="nav-list nav-list-mobile">
                    <li class="nav-item">
                        <div class="mobile-menu">
                            <span class="line line-top"></span>
                            <span class="line line-bottom"></span>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a href="/" class="nav-link nav-link-apple"></a>
                    </li>
                    <li class="nav-item">
                        <a href="/" class="nav-link nav-link-bag"></a>
                    </li>
                </ul>
                <!-- /.nav-list nav-list-mobile -->

                <ul class="nav-list nav-list-larger">
                    <li class="nav-item nav-item-hidden">
                        <a href="/" class="nav-link nav-link-apple"></a>
                    </li>
                    <li class="nav-item">
                        <a href="/" class="nav-link">Home</a>
                    </li>
                    <li class="nav-item">
                        <a href="/shop" class="nav-link">Shop</a>
                    </li>
                    <li class="nav-item">
                        <a href="/" class="nav-link">About Us</a>
                    </li>
                    <li class="nav-item">
                        <a href="/" class="nav-link">Contact</a>
                    </li>



                    <li class="nav-item">
                        <a href="/" class="nav-link nav-link-search"></a>
                    </li>
                    <li class="nav-item">
                        <a href="/" class="nav-link nav-link-bag"></a>
                    </li>
                </ul>
                <!-- /.nav-list nav-list-larger -->
            </nav>
        </div>
@endsection

