@extends('layouts.app')

<nav class="navbar navbar-expand-md navbar-light navbar-laravel">
    <div class="container">
        <a class="navbar-brand" href="{{ url('home') }}">
            {{ config('app.name', 'Laravel') }}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">

            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @if (Auth::guard('admin')->check() || Auth::guard()->check())
                    <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                @if (Auth::guard('admin')->check())
                                    {{ Auth::admin()->name }}
                                @else 
                                    {{ Auth::user()->name }} 
                                @endif
                                <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                @if (Auth::guard('admin')->check())
                                    <a class="dropdown-item" href="/admin/dashboard">Dashboard</a>
                                @else   
                                    <a class="dropdown-item" href="/dashboard">My-Profile</a> 
                                @endif
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                @else
                <li class="nav-item dropdown">
                        <a id="linkDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ __('Login') }}
                            <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu" aria-labelledby="linkDropdown">
                            <a class="dropdown-item" href="{{ route('login') }}">
                                {{ __('User') }}
                            </a>
                            <a class="dropdown-item" href="{{ route('admin.login') }}">
                                {{ __('Admin') }}
                            </a>
                        </div>
                    </li>
                    <li><a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a></li>
                @endif
            </ul>
        </div>
    </div>
</nav>