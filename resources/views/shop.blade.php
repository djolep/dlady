@extends('layouts.app')
@section('title',"Shop")
@section('content')
<div class="container-fluid" style="width: 80%;">
    <div class="row">
        <div class="col-sm-2" >
            <h3 class="shopHeader" style="margin-bottom: 50px;">Category</h3>
               
                <ul class="list-group">
                    <li class="nav-link"> <a class="nav-link category_link" href="{{route('shop.index')}}">All products</a> </li>
                </ul>
            
                @foreach ($categories as $category)
                    <ul class="list-group">
                    <li class="nav-link"> <a class="nav-link category_link" href="{{route('shop.index',['category'=>$category->name])}}">{{$category->name}}</a> </li>
                    </ul>
                @endforeach
                
        </div>
        <div class="col-sm-10">
            <h3 class="text-center shopHeader" style="margin-bottom:50px;">{{$categoryName}}</h3>
                <div class="row">
                    
                        @forelse ($products as $product)
                            <div class="col-xl-3 col-lg-3 col-sm-6"  style="margin: 10px;">
                                <div class="shadow" style="height: 100%;">
                                    <a href="/product.show/{{$product->id}}"><img src="/storage/Product_images/{{$product->image}}" class="card-img-top" style="margin-bottom: 10px;" alt=""></a>
                                    <div class="card-title">
                                        @if($product->discount)
                                            <button class="btn btn-danger align-right" style = "pointer-events: none;">{{$product->discount}}%</button>
                                        @else
                                            <button class="btn btn-danger align-right" style = "visibility: hidden">{{$product->discount}}%</button>
                                        @endif
                                        <a href="/product.show/{{$product->id}}"><h4 class="card-title text-center nav-link">{{$product->name}}</h4></a>
                                    </div>
                                    <div class="card-subtitle">
                                        <p class="text-center">{{$product->subtitle}}</p>
                                    </div>
                                    <div class="card-footer text-center">
                                        @if($product->discount)
                                            <button class="btn " style= "text-decoration: line-through; pointer-events: none; font-size: 12px;">{{$product->price }} din</button>
                                            <button class="btn " style = "pointer-events: none;">{{$product->price = $product->price + ($product->discount / 100) * $product->price }} din</button>
                                        @else
                                            <button class="btn " style = "pointer-events: none;">{{$product->price }} din</button>
                                        @endif
                                        <form action="{{ route('cart.store')}}" method="POST">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="id" value="{{$product->id}}">
                    
                                            <input type="hidden" name="name" value="{{$product->name}}">
                    
                                            <input type="hidden" name="design" value="{{$product->design}}">
                    
                                            <input type="hidden" name="image" value="{{$product->image}}">
                    
                                            <input type="hidden" name="price" value="{{$product->price}}">
                    
                                            <button type="submit" class="btn btn-light btn-md mr-1 mb-2"><i class="fas fa-shopping-cart pr-2"></i>Add to cart</button>
                                            
                                        </form>
                                    </div>
                                </div>
                                </div>
                        @empty
                            <div>No items found!</div>
                        @endforelse 
                    
                </div>   
                      
                
            {{$products->links()}}
        </div>
    </div>
</div>
   
    
@endsection