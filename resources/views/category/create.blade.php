@extends('layouts.app')
@section('content')
<div class=""> 
    <h1 class="text-center header font-weight-bold">Add category</h1>
</div>
<div class="alert " role="alert">
    @if(session()->has('message'))
        <div class="alert alert-success text-center">
            {{ session()->get('message') }}
        </div>
    @endif
</div>
<form class="container " method="post" action="/category" enctype="multipart/form-data">
    {{ csrf_field() }}   

    <div class="form-group col-md-8">
        <label for="category">Category</label>
        <input class="form-control col-md-8" type="text" name="name" placeholder="Enter category">
    </div>

    <div class="form-group col-md-8">
        <label for="picture">Add Category Image</label>
        <input type="file" name="image" multiple class="form-control-file " >
    </div>

    <div class="form-group col-md-8">
        <input class="btn btn-success" type="submit" name="submit" value="Add category">
        <button class="btn btn-success"><a class=" text-white" href="/product.create">Add product</a></button>
    </div>

</form>
@endsection
