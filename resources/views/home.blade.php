    @extends('layouts.app')
    @section('title',"Home")
@section('content')
    <div class="">
        
        <div class="container category ">
            <div class="row"> 
              @foreach($categories as $category)
                  <div class="col-4 card-desk text-center">
                    <a href="{{route('shop.index',['category'=>$category->name])}}"><img src="/storage/category_images/{{$category->image}}" alt="..." class="img-thumbnail" style="width: 450px; height: 313px;"></a>
                      
                      <button class="btn"><a class="text-center nav-link" href="{{route('shop.index',['category'=>$category->name])}}">{{$category->name}}</a></button>
                  </div>
              @endforeach
               
               <!-- <div class="col-4 card-desk ">
                   <img src="images/bag.jpg" alt="..." class="img-thumbnail">
                   <a class="text-center" href="/">Maske</a>
               </div>
               <div class="col-4 card-desk ">
                   <img src="images/bag.jpg" alt="..." class="img-thumbnail">
               </div>
               <div class="col-4 card-desk ">
                   <img src="images/bag.jpg" alt="..." class="img-thumbnail">
               </div>
               <div class="col-4 card-desk ">
                   <img src="images/bag.jpg" alt="..." class="img-thumbnail">
               </div>
               <div class="col-4 card-desk ">
                   <img src="images/bag.jpg" alt="..." class="img-thumbnail">
               </div>
            </div> -->
        </div>
    </div>
       
            

        <div class="container section-product">
            <h3 class="text-center title font-weight-bold">Best seller product</h3>
            <div class="row">
                <div class="col-6 ">
                    <img src="/images/shopping.jpg" class="rounded float-left " alt="200x200">
                </div>

                <div class="col-6">
                    <div class="description">
                        <p>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Adipisci et minima expedita animi, voluptatibus vitae quidem alias tempore enim repellendus quas itaque iusto, doloremque incidunt magnam nemo cum dolorum sed.
                        </p><br><br>
                        <button class="btn btn-success">Bay Now</button>
                    </div>
                </div>
            </div>


            <div class="views">
                <div class="view-header">
                    <h3 class="text-center font-weight-bold">Costumers Review</h3>
                </div>
                <div class="view-body">
                    <p class="font-italic">"Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quos praesentium iure ipsum necessitatibus laborum reprehenderit iste harum recusandae culpa enim laboriosam optio qui fuga, doloribus vitae et provident possimus nam! Lorem ipsum, dolor sit amet consectetur adipisicing elit. Officia dicta quo, labore est tenetur sequi voluptate aut doloribus! Culpa aliquid velit esse consequuntur doloremque dolore, ex ut. Culpa, itaque dolorum."</p>
                </div>
            </div>
        </div>


        <!-- Footer -->
<footer class="page-footer font-small mdb-color pt-4">

    <!-- Footer Links -->
    <div class=" text-center text-md-left">
  
      <!-- Footer links -->
      <div class="row text-center text-md-left mt-3 pb-3">
  
        <!-- Grid column -->
        <div class="col-md-3 col-lg-3 col-xl-3 mx-auto mt-3">
          <h6 class=" mb-4 font-weight" style="font-family: Adine Kirnberg; font-size: 78px;">Dlady</h6>
          <p>Here you can use rows and columns to organize your footer content. Lorem ipsum dolor sit amet,
            consectetur
            adipisicing elit. </p>
            
        </div>
        <!-- Grid column -->
  
        <hr class="w-100 clearfix d-md-none">
  
        <!-- Grid column -->
        <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mt-3">
          <h6 class="text-uppercase mb-4 font-weight-bold">Products</h6>
          <p>
            <a href="#!">MDBootstrap</a>
          </p>
          <p>
            <a href="#!">MDWordPress</a>
          </p>
          <p>
            <a href="#!">BrandFlow</a>
          </p>
          <p>
            <a href="#!">Bootstrap Angular</a>
          </p>
        </div>
        <!-- Grid column -->
  
        <hr class="w-100 clearfix d-md-none">
  
        <!-- Grid column -->
        <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mt-3">
          <h6 class="text-uppercase mb-4 font-weight-bold">Useful links</h6>
          <p>
            <a href="#!">Your Account</a>
          </p>
          <p>
            <a href="#!">Become an Affiliate</a>
          </p>
          <p>
            <a href="#!">Shipping Rates</a>
          </p>
          <p>
            <a href="#!">Help</a>
          </p>
        </div>
  
        <!-- Grid column -->
        <hr class="w-100 clearfix d-md-none">
  
        <!-- Grid column -->
        <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mt-3">
          <h6 class="text-uppercase mb-4 font-weight-bold">Contact</h6>
          <p>
            <i class="fas fa-home mr-3"></i> Beograd, Svetolika Rankovica, 4</p>
          <p>
            <i class="fas fa-envelope mr-3"></i> dlady@gmail.com</p>
          <p>
            <i class="fas fa-phone mr-3"></i> + 01 234 567 88</p>
          <p>
            <i class="fas fa-print mr-3"></i> + 01 234 567 89</p>
        </div>
        <!-- Grid column -->
  
      </div>
      <!-- Footer links -->
  
      <hr>
  
      <!-- Grid row -->
      <div class="row d-flex align-items-center">
  
        <!-- Grid column -->
        <div class="col-md-7 col-lg-8">
  
          <!--Copyright-->
          <p class="text-center ">© 2020 Copyright:
            <a href="https://dlady.com/">
              <strong> dlady.com</strong>
            </a>
          </p>
  
        </div>
        <!-- Grid column -->
  
        <!-- Grid column -->
        <div class="col-md-5 col-lg-4 ml-lg-0">
  
          <!-- Social buttons -->
          <div class="text-center text-md-left">
            <ul class="list-unstyled list-inline">
              <li class="list-inline-item">
                <a class="btn-floating btn-sm rgba-white-slight mx-1">
                  <i class="fab fa-facebook-f"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a class="btn-floating btn-sm rgba-white-slight mx-1">
                  <i class="fab fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a class="btn-floating btn-sm rgba-white-slight mx-1">
                  <i class="fab fa-google-plus-g"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a class="btn-floating btn-sm rgba-white-slight mx-1">
                  <i class="fab fa-linkedin-in"></i>
                </a>
              </li>
            </ul>
          </div>
  
        </div>
        <!-- Grid column -->
  
      </div>
      <!-- Grid row -->
  
    </div>
    <!-- Footer Links -->
  
  </footer>
  <!-- Footer -->



    
@endsection

