@extends('layouts.app')
@section('content')

<div class=""> 
    <h1 class="text-center header">Add product</h1>
</div>

<div class="alert " role="alert">
    @if(session()->has('message'))
        <div class="alert alert-success text-center">
            {{ session()->get('message') }}
        </div>
    @endif
</div>

<form id="upload" class="container" action="/product/{{$product->id}}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}   
    @method('PUT')
    <div class="row">
        <div class="col-6">
            <div class="form-group">
            <label for="category">Category</label>
            <select class="form-control col-md-6" name="category_id" >
                @foreach ($categories as $category)
                <option disabled selected hidden>Select category</option>
                    <option name="category_id" value="{{ $category->id }}">
                        {{$category->name}}
                    </option>
                @endforeach                
              </select>
              
        </div>
        <div class="form-group">
            <button class="btn btn-success col-sm-6"><a class="text-white" href="/category.create">Add new category</a></button>
            </div>
        <div class="form-group">
            <label for="name">Product Name</label>
            <input class="form-control col-md-6" type="text" name="name" value="{{$product->name}}">
        </div>
    
        <div class="form-group">
            <label for="subtitle">About</label>
            <input class="form-control col-md-6" type="text" name="subtitle" value="{{$product->subtitle}}">
        </div>
    
        <div class="form-group">
            <label for="description" >Description</label>
            <textarea class="form-control col-md-6" type="text" name="description" value="">{{$product->description}}</textarea>
        </div>
    
        <div class="form-group">
            <label for="Price">Price</label>
            <input class="form-control col-md-6" type="number" name="price" value="{{$product->price}}">
            <input type="number" class="form-control col-md-6" name="discount" value="{{$product->discount}}">

            {{-- <input type="number" class="form-control col-sm-2" name="discount" placeholder="Discount"> --}}
        </div>
        <div class="form-group">
            
        {{-- <input type="hidden"  name="discounted_price" value="{{  }}"> --}}
        </div>
        <div class="form-group">
            <label for="picture">Add Image</label>
            <input type="file" name="image" multiple class="form-control-file " value="">
        </div>

        <div>
            <input class="btn btn-success text-white" type="submit" name="submit" value="Update Product">
        </div>
        </div>
        <div class="col-6">
            <div class="form-group">
                <label for="subtitle">Design</label>
                <input class="form-control col-md-6" type="text" name="design" value="{{$product->design}}">
            </div>

            <div class="form-group">
                <label for="subtitle">Color</label>
                <input class="form-control col-md-6" type="text" name="color" value="{{$product->color}}">
            </div>

            <div class="form-group">
                <label for="subtitle">Delivery</label>
                <input class="form-control col-md-6" type="text" name="delivery" value="{{$product->delivery}}">
            </div>
        </div>
    </div>  
        
       
    </form>

    {{-- <script>
        var form = document.getElementById('upload');
        var request = new XMLHttpRequest();

        form.addEventListener('submit', function(e){
            e.preventDefault();
            var formData = new FormData(form);
            request.open('post', '/product');
            request.addEventListener("load", ransverComplete);
            request.send(formData);
        });

        function transverComplete(data){
            response = JSON.parse(data.currentTarget.response);
        }
    </script> --}}
@endsection
