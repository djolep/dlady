@extends('layouts.app')

@section('content')
		{{-- <div id="wrapper">
			<div id="page" class="container">
				<div id="content">
					<div class="title">
						<h2>{{$product->name}}</h2>
				</div>
				<div class="image image-full">
					<img src="/storage/Product_images/{{$product->image}}" alt="" class="image image-full" /> 
				</div>
					
					
					<p>{{ $product->description }}</p>
					

					
				</div>
				
			</div>
		</div> 
		--}}

		<!--Section: Block Content-->
		<div class="container" style="margin-top: 100px;">

			<div class="row">
			<div class="col-md-6 mb-4 mb-md-0">
		
				<div id="mdb-lightbox-ui"></div>
		
				<div class="mdb-lightbox">
		
				<div class="row product-gallery mx-1">
		
					<div class="col-12 mb-0">
					<figure class="view overlay rounded z-depth-1 main-img">
						<a href=""
						data-size="400x300">
						<img src="/storage/Product_images/{{$product->image}}"
							class="img-fluid z-depth-1">
						</a>
					</figure>
					
					</div>
					<div class="col-12">
					<div class="row">
						<div class="col-3">
						<div class="view overlay rounded z-depth-1 gallery-item">
							<img src="https://mdbootstrap.com/img/Photos/Horizontal/E-commerce/Vertical/12a.jpg"
							class="img-fluid">
							<div class="mask rgba-white-slight"></div>
						</div>
						</div>
						<div class="col-3">
						<div class="view overlay rounded z-depth-1 gallery-item">
							<img src="https://mdbootstrap.com/img/Photos/Horizontal/E-commerce/Vertical/13a.jpg"
							class="img-fluid">
							<div class="mask rgba-white-slight"></div>
						</div>
						</div>
						<div class="col-3">
						<div class="view overlay rounded z-depth-1 gallery-item">
							<img src="https://mdbootstrap.com/img/Photos/Horizontal/E-commerce/Vertical/14a.jpg"
							class="img-fluid">
							<div class="mask rgba-white-slight"></div>
						</div>
						</div>
						<div class="col-3">
						<div class="view overlay rounded z-depth-1 gallery-item">
							<img src="https://mdbootstrap.com/img/Photos/Horizontal/E-commerce/Vertical/15a.jpg"
							class="img-fluid">
							<div class="mask rgba-white-slight"></div>
						</div>
						</div>
					</div>
					</div>
				</div>
		
				</div>
		
			</div>
			<div class="col-md-6">
		
				<h3>{{$product->name}}</h3>
				<p class="mb-2 text-muted text">{{$product->subtitle}}</p>
				<ul class="rating">
				<li>
					<i class="fas fa-star fa-sm text-primary"></i>
				</li>
				<li>
					<i class="fas fa-star fa-sm text-primary"></i>
				</li>
				<li>
					<i class="fas fa-star fa-sm text-primary"></i>
				</li>
				<li>
					<i class="fas fa-star fa-sm text-primary"></i>
				</li>
				<li>
					<i class="far fa-star fa-sm text-primary"></i>
				</li>
				</ul>
				@if($product->discount)
					<button class="btn btn-danger align-right" style = "pointer-events: none;">{{$product->discount}}%</button>
					<p><span class="mr-1"><strong class="price" style= "text-decoration: line-through; font-size: 20px;">{{$product->price}} din</strong></span></p>
					<p><span class="mr-1"><strong class="price">{{$product->price = $product->price + ($product->discount / 100) * $product->price}} din</strong></span></p>
				@else
				<p><span class="mr-1"><strong class="price">{{$product->price}} din</strong></span></p>
				@endif
				<p class="pt-1">{{$product->description}}</p>
				<div class="table-responsive">
				<table class="table table-sm table-borderless mb-0">
					<tbody>
					<tr>
						<th class="pl-0 w-25" scope="row"><strong>Design</strong></th>
						<td>{{$product->design}}</td>
					</tr>
					<tr>
						<th class="pl-0 w-25" scope="row"><strong>Color</strong></th>
						<td>{{$product->color}}</td>
					</tr>
					<tr>
						<th class="pl-0 w-25" scope="row"><strong>Delivery</strong></th>
						<td>{{$product->delivery}}</td>
					</tr>
					</tbody>
				</table>
				</div>
				<br>
				<div class="table-responsive mb-2">
				<table class="table table-sm table-borderless">
					<tbody>
					<tr>
						<td class="pl-0 pb-0 w-25">Quantity</td>
						<td class="pb-0"></td>
					</tr>
					<tr>
						<td class="pl-0">
						<div class="def-number-input number-input safari_only mb-0">
							<button onclick="this.parentNode.querySelector('input[type=number]').stepDown()"
							class="minus"></button>
							<input class="quantity" min="0" name="quantity" value="1" type="number">
							<button onclick="this.parentNode.querySelector('input[type=number]').stepUp()"
							class="plus"></button>
						</div>
						</td>
						<td>
						{{-- <div class="mt-1">
							<div class="form-check form-check-inline pl-0">
							<input type="radio" class="form-check-input" id="small" name="materialExampleRadios"
								checked>
							<label class="form-check-label small text-uppercase card-link-secondary"
								for="small">Small</label>
							</div>
							<div class="form-check form-check-inline pl-0">
							<input type="radio" class="form-check-input" id="medium" name="materialExampleRadios">
							<label class="form-check-label small text-uppercase card-link-secondary"
								for="medium">Medium</label>
							</div>
							<div class="form-check form-check-inline pl-0">
							<input type="radio" class="form-check-input" id="large" name="materialExampleRadios">
							<label class="form-check-label small text-uppercase card-link-secondary"
								for="large">Large</label>
							</div> --}}
						</div>
						</td>
					</tr>
					</tbody>
				</table>
				</div>
				{{-- <a type="button" class="btn btn-primary btn-md mr-1 mb-2" href="cart">Bay Now</a> --}}
					<form action="{{ route('cart.store')}}" method="POST">
						{{ csrf_field() }}
						<input type="hidden" name="id" value="{{$product->id}}">

						<input type="hidden" name="name" value="{{$product->name}}">

						<input type="hidden" name="design" value="{{$product->design}}">

						<input type="hidden" name="image" value="{{$product->image}}">

						<input type="hidden" name="price" value="{{$product->price}}">

						<button type="submit" class="btn btn-light btn-md mr-1 mb-2"><i class="fas fa-shopping-cart pr-2"></i>Add to cart</button>
						
					</form>
				
			</div>
			</div>
		
		</div>
		<!--Section: Block Content-->
@endsection