@extends('layouts.app')


@section('content')
    <div class="container-fluid" style="width: 75%">            
        @foreach ($orders as $order)
            <div class="card">
                <div class="row">
                    <div class="col-6">
                        <li class="list-group-item">{{$order->firstName}} {{$order->lastName}}</li>
                        <li class="list-group-item">{{$order->email}}</li>
                        <li class="list-group-item">{{$order->phone}}</li>
                        <li class="list-group-item">{{$order->address}}</li>
                        <li class="list-group-item">{{$order->country}}</li>
                        <li class="list-group-item">{{$order->zip}}</li>
                        <li class="list-group-item">{{$order->state}}</li>
                    </div>
                    <div class="col-6">
                        <table id="table" class="table">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Product Name</th>
                                    <th scope="col">Product model</th>
                                    <th scope="col">Product price</th>
                                    <th scope="col">Quantity</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                @foreach ($order->products as $product)
                                    <tr>
                                        <td scope="row">{{$product->id}}</td>
                                        <td>{{$product->name}}</td>
                                        <td>{{$product->design}}</td>
                                        <td class="subjects">{{$product->price}}</td>
                                        <td>{{$product->pivot->quantity}}</td>
                                    </tr>
                                 @endforeach
                               <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td id="val"><strong>{{Cart::total()}} din</strong></td>  
                                </tr>
                               
                            </tbody>

                            

                            </table>
                        {{-- @foreach ($order->products as $product)

                            <div><a href="/product.show/{{$product->id}}"><img class="img-responsive" src="/storage/Product_images/{{$product->image}}" alt="preview" width="60" height="50"></a> {{$product->name}}  {{$product->design}}  {{$product->price}}din</div>
                            <div></div>
                            <div></div>
                            <div></div>
                        @endforeach --}}
                        </div>
                    
                </div>
            </div><br>
        @endforeach        
    </div>
@endsection