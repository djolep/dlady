<?php
use App\Product;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create([
            'name' => 'Macbook pro',
            'slug' => 'macbook-pro',
            'details' => '15 inch, 1TB SSD, 32GB RAM',
            'price' => '135000',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
            
        ]);

        Product::create([
            'name' => 'Macbook pro',
            'slug' => 'macbook-pro 2',
            'details' => '15 inch, 1TB SSD, 32GB RAM',
            'price' => '135000',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
            
        ]);

        Product::create([
            'name' => 'Macbook pro',
            'slug' => 'macbook-pro 3',
            'details' => '15 inch, 1TB SSD, 32GB RAM',
            'price' => '135000',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
            
        ]);
    }
}
